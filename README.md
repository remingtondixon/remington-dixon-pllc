Remington & Dixon, PLLC is dedicated to providing our clients with top-notch representation in the areas of criminal defense, family law, professional license defense, traffic violations, personal injury, and unemployment appeals.

Address: 135 Perrin Pl, Suite 200, Charlotte, NC 28207, USA

Phone: 704-247-7110

Website: https://remingtondixon.com
